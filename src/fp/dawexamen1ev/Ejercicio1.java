package fp.dawexamen1ev;

import java.io.*;

public class Ejercicio1 {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Introduce una cadena de caracteres: ");

		String cadena = reader.readLine();

		cadena.toLowerCase();

		char[] vector = cadena.toCharArray();

		for (int i = 0; i < vector.length; i++) {

			System.out.print(vector[i] + " ");

		}

	}

}
